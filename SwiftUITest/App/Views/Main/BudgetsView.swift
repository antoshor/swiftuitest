//
//  BudgetsView.swift
//  SwiftUITest
//
//  Created by Krasilnikov Anton on 10.01.2024.
//

import SwiftUI

struct BudgetsView: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 16) {
            Text("My Budgets")
                .font(.headline)
                .fontWeight(.semibold)
                .padding(.top, 32)
                .foregroundStyle(Color(UIColor(red: 0.76, green: 0.72, blue: 0.98, alpha: 1)))
                
            
            VStack(alignment: .leading) {
                Text("You have")
                    .fontWeight(.medium)
              
                Text("29,880")
                    .font(.title)
                    .bold()
                    .padding(.top, 5)
                
                Text("Left out of N80,888 budgeted")
                    .fontWeight(.medium)
                    .opacity(0.8)
                
                ProgressView(value:  0.75)
                    .tint(Color(UIColor(red: 0.2, green: 0.99, blue: 0.39, alpha: 1)))
                    .padding(.vertical, 24)
                
                HStack {
                    Text("😱")
                    Text("Sapa go soon catch you bros, calm down!!")
                        .fontWeight(.medium)
                }
            }
            .foregroundStyle(.white)
            .padding()
            .background(Color(UIColor(red: 0.26, green: 0.17, blue: 0.93, alpha: 1)))
            .cornerRadius(16)
        }
    }
}

#Preview {
    BudgetsView()
}
