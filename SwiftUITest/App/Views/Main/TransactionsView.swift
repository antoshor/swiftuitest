//
//  TransactionsView.swift
//  SwiftUITest
//
//  Created by Krasilnikov Anton on 09.01.2024.
//

import SwiftUI

struct TransactionsView: View {
    var body: some View {
        HStack(spacing: 26) {
            DiscountImage(size: CGSize(width: 38, height: 38), image: Image("tabler_discount"))
            
            VStack(alignment: .leading) {
                Text("Sort your transactions")
                    .font(.custom("Inter", size: 14))
                    .fontWeight(.semibold)
                    .foregroundStyle(.white)
                
                Text("Get points for sorting your transactions")
                    .font(.custom("Inter", size: 14))
                    .opacity(0.75)
                    .foregroundStyle(.white)
            }
            
            Spacer()
        }
        .padding()
        .background(Color(UIColor(red: 0.13, green: 0.06, blue: 0.64, alpha: 1)))
        .listRowInsets(EdgeInsets())
        .cornerRadius(16)
    }
}

#Preview {
    TransactionsView()
}
