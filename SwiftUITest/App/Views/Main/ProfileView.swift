//
//  ProfileView.swift
//  SwiftUITest
//
//  Created by Krasilnikov Anton on 09.01.2024.
//

import SwiftUI

struct ProfileView: View {
    var bankModel = ["National Bank": "12,000.00", "Habib Bank": "950.00", "Utilities": "1,050.00"]
    var body: some View {
        VStack {
            VStack(spacing: 0) {
                ProfileImage(image: Image("profile"))
                
                Text("Your available balance is")
                    .font(.headline)
                    .foregroundStyle(.white)
                    .opacity(0.8)
                    .fontWeight(.medium)
                    .padding(.top, 14)
                
                Text("20,983")
                    .font(.title)
                    .fontWeight(.heavy)
                    .foregroundStyle(.white)
                    .padding(.top, 8)
                
                Text("By this time last month, you spent slightly higher (22,719)")
                    .font(.headline)
                    .fontWeight(.medium)
                    .foregroundStyle(.white)
                    .lineLimit(2)
                    .multilineTextAlignment(.center)
                    .padding(.top, 16)
            }
            .padding()
            createVStack()
        }
        .background(Color(UIColor(red: 0.13, green: 0.06, blue: 0.64, alpha: 1)))
        .cornerRadius(24)
    }
    
    private func createVStack() -> some View {
        VStack(spacing: 31) {
            ForEach(bankModel.sorted(by: >), id: \.key) { key, value in
                HStack {
                    Text(key)
                        .font(.body)
                    Spacer()
                    Text(bankModel[key]!)
                        .font(.body)
                }
            }
        }
        .padding()
        .foregroundColor(.white)
    }
}

#Preview {
    ProfileView()
}
