//
//  ContentView.swift
//  SwiftUITest
//
//  Created by Krasilnikov Anton on 09.01.2024.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(spacing: 16) {
                    ZStack(alignment: .topTrailing) {
                        ProfileView()
                        
                        NavigationLink(destination: AccountView()) {
                            Image(systemName: "chevron.forward.circle")
                                .foregroundStyle(.gray)
                        }
                        .offset(x: -20, y: 20)
                    }
                    
                    TransactionsView()
                    
                    BudgetsView()
                }
                .padding()
                .background(Color(UIColor(red: 0.17, green: 0.08, blue: 0.87, alpha: 1)))
            }
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarLeading) {
                    VStack(alignment: .leading) {
                        Text("Hello Bilal")
                            .fontWeight(.semibold)
                        Text("Your finances are looking good")
                            .opacity(0.8)
                            .fontWeight(.medium)
                    }
                    .foregroundStyle(.white)
                }
                
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    Button {
                        
                    } label: {
                        Image(systemName: "magnifyingglass")
                            .foregroundStyle(.gray)
                    }
                    
                    Button {
                        
                    } label: {
                        Image(systemName: "bell")
                            .foregroundStyle(.gray)
                    }
                }
            }
            .background(Color(UIColor(red: 0.17, green: 0.08, blue: 0.87, alpha: 1)))
        }
       
    }
    
}

#Preview {
    ContentView()
}
