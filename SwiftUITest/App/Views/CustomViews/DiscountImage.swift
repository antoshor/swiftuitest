//
//  DiscountImage.swift
//  SwiftUITest
//
//  Created by Krasilnikov Anton on 10.01.2024.
//

import SwiftUI

struct DiscountImage: View {
    
    var size: CGSize
    var image: Image
    
    var body: some View {
        ZStack {
            image
                .resizable()
                //.aspectRatio(contentMode: .fill)
                .frame(width: size.width, height: size.height)
                .background(Color(UIColor(red: 0.35, green: 0.27, blue: 0.93, alpha: 1)))
                .cornerRadius(10)
            
            Circle()
                .frame(width: size.width / 3)
                .foregroundStyle(.yellow)
                .offset(x: size.width / 2, y: -size.height / 2)
        }
    }
}

#Preview {
    DiscountImage(size: CGSize(width: 52, height: 52), image: Image("tabler_discount"))
}
