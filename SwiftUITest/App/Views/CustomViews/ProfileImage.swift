//
//  ProfileImage.swift
//  SwiftUITest
//
//  Created by Krasilnikov Anton on 09.01.2024.
//

import SwiftUI

struct ProfileImage: View {
    
    var image: Image
    var body: some View {
        
            image
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 52, height: 52)
                .background(Color(UIColor(red: 0.74, green: 0.84, blue: 1, alpha: 1)))
                .clipShape(Circle())
    }
}

#Preview {
    ProfileImage(image: Image("profile"))
}
