//
//  SwiftUITestApp.swift
//  SwiftUITest
//
//  Created by Krasilnikov Anton on 09.01.2024.
//

import SwiftUI

@main
struct SwiftUITestApp: App {
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
