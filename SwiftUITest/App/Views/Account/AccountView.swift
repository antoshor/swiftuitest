//
//  AccountView.swift
//  SwiftUITest
//
//  Created by Krasilnikov Anton on 10.01.2024.
//

import SwiftUI

struct AccountView: View {
    var body: some View {
        VStack {
            Spacer()
            
            Text("Your available balance is")
                .font(.subheadline)
                .foregroundStyle(.gray)
            
            Text("20,983")
                .bold()
                .font(.title)
            
            SliderView()
                .padding(.top, 94)
            
            Spacer()
            Button {
            } label: {
                ZStack {
                    Capsule()
                        .frame(height: 62)
                    Text("+ Add new account")
                        .foregroundStyle(.white)
                }
            }
        }
        .padding()
    }
}

#Preview {
    AccountView()
}
