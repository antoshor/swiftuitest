//
//  SliderView.swift
//  SwiftUITest
//
//  Created by Krasilnikov Anton on 10.01.2024.
//

import SwiftUI

struct SliderView: View {
    
    var count = [1, 2, 2, 3]
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(count, id: \.self) {_ in
                    VStack {
                        Image("bank")
                        Text("National Bank")
                        Text("12,000.00")
                    }
                    .padding()
                    .overlay(
                        RoundedRectangle(cornerRadius: 8)
                            .stroke(.blue, lineWidth: 2)
                    )
                    
                }
            }
        }
    }
}

#Preview {
    SliderView()
}
